import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'ionic-poke-master',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
