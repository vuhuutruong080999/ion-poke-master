import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  private apiUrl = environment.api;
  constructor(private http: HttpClient) {}
  getMethod(url: string, param?: any): Observable<any> {
    return this.http.get(this.apiUrl + url, {
      params: new HttpParams({ fromObject: param }),
    });
  }
}
