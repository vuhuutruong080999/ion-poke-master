import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetailMasterRoutingModule } from './detail-master-routing.module';
import { DetailMasterComponent } from './detail-master.component';

@NgModule({
  declarations: [DetailMasterComponent],
  imports: [CommonModule, DetailMasterRoutingModule, IonicModule],
})
export class DetailMasterModule {}
