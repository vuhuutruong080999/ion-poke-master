import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-pokemon',
  templateUrl: './pokemon.component.html',
  styleUrls: ['./pokemon.component.scss'],
})
export class PokemonComponent implements OnInit {
  name = '';
  pokemon: any;
  constructor(private route: ActivatedRoute, private apiService: ApiService) {}

  ngOnInit(): void {
    this.name = this.route.snapshot.paramMap.get('name');
    this.getData();
  }
  getData() {
    this.apiService
      .getMethod(`pokemon/${this.name}`)
      .subscribe((res) => (this.pokemon = res));
  }
}
