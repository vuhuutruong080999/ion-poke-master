import { DetailMasterComponent } from './detail-master.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: DetailMasterComponent,
    children: [
      {
        path: 'pokemon/:name',
        loadChildren: () =>
          import('./pokemon/pokemon.module').then((m) => m.PokemonModule),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailMasterRoutingModule {}
