import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PokemonComponent } from './pokemon/pokemon.component';

@NgModule({
  declarations: [PokemonComponent],
  imports: [CommonModule, IonicModule],
})
export class ModalsModule {}
