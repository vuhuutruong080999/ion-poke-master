import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListRoutingModule } from './list-routing.module';
import { ListComponent } from './list.component';
import { PokemonCardModule } from './shared/pokemon-card/pokemon-card.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [ListComponent],
  imports: [
    CommonModule,
    ListRoutingModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    PokemonCardModule,
  ],
})
export class ListModule {}
