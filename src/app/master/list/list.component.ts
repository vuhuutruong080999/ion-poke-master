import { ApiService } from './../../services/api.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder } from '@angular/forms';
import { debounceTime, map } from 'rxjs/operators';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  listData = [];
  listPokemon = [];
  listSearch = [];
  isSearch = false;
  offset = 0;
  limit = 20;
  pokemon: any;
  search: FormControl = this.fb.control('');
  constructor(private apiService: ApiService, private fb: FormBuilder) {
    this.search.valueChanges
      .pipe(
        map((e) => e),
        debounceTime(1000)
      )
      .subscribe((res) => {
        if (res === '') {
          this.offset = 0;
          this.isSearch = false;
          this.loadMore();
        } else {
          this.isSearch = true;
          this.onSearch(res);
        }
      });
  }

  ngOnInit(): void {
    this.getData();
  }
  getData() {
    this.apiService.getMethod('pokemon', { limit: 1126 }).subscribe((res) => {
      this.listData = res.results;
      this.loadMore();
    });
  }
  loadMore(e?: any) {
    if (this.isSearch) {
      this.listPokemon = this.listSearch.slice(
        this.offset,
        this.offset + this.limit
      );
    } else {
      this.listPokemon = this.listData.slice(
        this.offset,
        this.offset + this.limit
      );
    }
    e?.target.complete();
    this.offset += this.limit;
  }
  onSearch(search) {
    this.offset = 0;
    this.listSearch = this.listData.filter(
      (res) => res.name.toLowerCase().indexOf(search) > -1
    );
    this.loadMore();
  }
}
