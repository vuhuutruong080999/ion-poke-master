import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MasterRoutingModule } from './master-routing.module';
import { MasterComponent } from './master.component';

@NgModule({
  declarations: [MasterComponent],
  imports: [CommonModule, MasterRoutingModule, IonicModule],
})
export class MasterModule {}
