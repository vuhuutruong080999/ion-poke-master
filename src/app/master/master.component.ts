import { Component, OnInit } from '@angular/core';
import { Route, Router } from '@angular/router';

@Component({
  selector: 'app-master',
  templateUrl: './master.component.html',
  styleUrls: ['./master.component.scss'],
})
export class MasterComponent implements OnInit {
  current = 'home';
  constructor(private router: Router) {}

  ngOnInit(): void {
    const routerSplit = this.router.url.split('/');
    this.current = routerSplit[routerSplit.length - 1];
  }
}
