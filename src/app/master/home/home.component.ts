import { ApiService } from './../../services/api.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { debounceTime, map } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  offset = 0;
  listPokemon: any[] = [];
  pokemon: any;
  search: FormControl = this.fb.control('');
  constructor(private apiService: ApiService, private fb: FormBuilder) {
    this.search.valueChanges
      .pipe(
        map((e) => e),
        debounceTime(1000)
      )
      .subscribe((res) => {
        if (res === '') {
          this.getData();
          this.pokemon = null;
        } else {
          this.onSearch(res);
        }
      });
  }

  ngOnInit(): void {
    this.getData();
  }
  getData(e?: any) {
    if (e) {
      e.target.complete();
    }
    const payload = { limit: 20, offset: this.offset };
    this.apiService.getMethod('pokemon', payload).subscribe((res) => {
      this.listPokemon = this.listPokemon.concat(res.results);
      this.offset += payload.limit;
    });
  }
  onSearch(search: string) {
    this.listPokemon = [];
    this.apiService.getMethod(`pokemon/${search}`).subscribe((res) => {
      this.pokemon = res;
    });
  }
}
