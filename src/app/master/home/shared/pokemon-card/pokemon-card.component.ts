import { ApiService } from './../../../../services/api.service';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-pokemon-card',
  templateUrl: './pokemon-card.component.html',
  styleUrls: ['./pokemon-card.component.scss'],
})
export class PokemonCardComponent implements OnInit {
  @Input() pokemon: any;
  pokemonDetail: any;
  constructor(private apiService: ApiService) {}

  ngOnInit(): void {
    this.getData();
  }
  getData() {
    this.apiService
      .getMethod(`pokemon/${this.pokemon.name}`)
      .subscribe((res) => {
        this.pokemonDetail = res;
      });
  }
}
