import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { MasterModule } from './master/master.module';
import { HomeModule } from './master/home/home.module';
import { ListModule } from './master/list/list.module';
import { DetailMasterModule } from './detail-master/detail-master.module';
import { PokemonModule } from './detail-master/pokemon/pokemon.module';
import { ModalsModule } from './modals/modals.module';

import { HttpClientModule } from '@angular/common/http';
import { ApiService } from './services/api.service';
@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    MasterModule,
    HomeModule,
    ListModule,
    DetailMasterModule,
    PokemonModule,
    ModalsModule,
    HttpClientModule,
  ],
  providers: [
    ApiService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
